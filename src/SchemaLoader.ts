import { FileSchemaLoaderStrategy } from '@themost/data';
import { resolve } from 'node:path';
import { ConfigurationBase, DataModelProperties} from '@themost/common';

export class SchemaLoader extends FileSchemaLoaderStrategy {

    constructor(config: ConfigurationBase) {
        super(config);
        this.setModelPath(resolve(__dirname, 'config/models'));
    }

    getModelDefinition(name: string): DataModelProperties {
        const model = super.getModelDefinition.bind(this)(name);
        if (model) {
            if (Array.isArray(model.eventListeners)) {
                model.eventListeners.forEach((eventListener: { type: string }) => {
                    if (eventListener.type.indexOf('.') === 0) {
                        eventListener.type = resolve(__dirname, eventListener.type);
                    }
                });
            }
            if (model.classPath && model.classPath.indexOf('.') === 0) {
                model.classPath = resolve(__dirname, model.classPath);
            }
        }
        return model;

    }

}