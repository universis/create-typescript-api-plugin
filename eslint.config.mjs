// @ts-check

import eslint from '@eslint/js';
import tseslint from 'typescript-eslint';

export default tseslint.config({
        ignores: [
            'node_modules',
            'dist',
            'coverage',
            'jest.config.js',
            'jest.setup.js',
            'rollup.config.mjs',
        ],
    },
    eslint.configs.recommended,
    tseslint.configs.recommended,
    tseslint.configs.strict,
    tseslint.configs.stylistic
);