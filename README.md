# create-typescript-api-plugin

Universis api plugin template for typescript

This template is a plugin for the [Universis API Server](https://gitlab.com/universis/universis-api). 
It is written in typescript and is intended to be used as a starting point for creating new plugins.

It contains an application service that is automatically loaded by the server and can be used to add new functionality to the server.

A plugin of Universis API server usually exports a service class which can be loaded using `services` section of the server configuration file:

```json
{
    "services": [
        {
            "serviceType": "@universis/api-plugin#Service"
        }
    ]
}
```

An application service is a class that extends the `ApplicationService` class:

```typescript
import {ApplicationBase, ApplicationService} from '@themost/common';
import {DataContext} from "@themost/data";

class MyService extends ApplicationService {
    constructor(app: ApplicationBase) {
        super(app);
    }

    async doSomething(context: DataContext) {
        // do something
    }
}
```

It also contains a schema loader that is used to load the additional schema files that are imported by the plugin.
A schema loader uses *.json schemas under `config/models` directory to load additional data models.

A schema loader should be included in the server configuration file:

```json
{
    "settings": {
      "schema": {
        "loaders": [
          {
            "loaderType": "@universis/api-plugin#SchemaLoader"
          }
        ]
      }
    }
}
```

A schema loader is a class that extends the `SchemaLoaderStrategy` class and can be used to load additional data models from the plugin:

```typescript
import {SchemaLoaderStrategy} from "@themost/data";
import {ConfigurationBase} from "@themost/common";

class SchemaLoader extends SchemaLoaderStrategy {
    constructor(configuration: ConfigurationBase) {
        super(configuration);
        // load models from config/models directory
        this.setModelPath(resolve(__dirname, 'config/models'));
    }

    // override getModelDefinition method
    getModelDefinition(name: string): DataModelProperties {
        // call super method to get model definition
        const model = super.getModelDefinition.bind(this)(name);
        if (model) {
            if (Array.isArray(model.eventListeners)) {
                // if model has event listeners
                model.eventListeners.forEach((eventListener: any) => {
                    // and event listener type starts with '.' (dot)
                    if (eventListener.type.indexOf('.') === 0) {
                        // resolve event listener type
                        eventListener.type = resolve(__dirname, eventListener.type);
                    }
                });
            }
            // do the same for classPath
            // if model has classPath and classPath starts with '.' (dot)
            if (model.classPath && model.classPath.indexOf('.') === 0) {
                // resolve classPath
                model.classPath = resolve(__dirname, model.classPath);
            }
        }
        return model;
    }
    
}

```

This template uses `jest` for testing. You can add and run tests using the following command:

```bash
npm test
```

It also uses `rollup` for bundling the plugin. You can build the plugin using the following command:

```bash
npm run build
```

