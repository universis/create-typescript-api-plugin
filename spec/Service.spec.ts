import { Service } from '../src';
import { ExpressDataApplication } from '@themost/express';
import { DataCacheStrategy } from '@themost/data';
import {ApplicationBase, ApplicationService} from "@themost/common";

interface DataCacheFinalize {
    finalize(): Promise<void>;
}

describe('Service', () => {
    let app: ExpressDataApplication;
    beforeAll(() => {
        app = new ExpressDataApplication();
    });
    afterAll(async () => {
        const cache: DataCacheFinalize = app.getConfiguration().getStrategy(DataCacheStrategy) as unknown as DataCacheFinalize;
        if (cache && typeof cache.finalize === 'function') {
            await cache.finalize();
        }
    })
    it('should create instance', async () => {
        const service = new Service(app as unknown as ApplicationBase);
        expect(service).toBeTruthy();
    });
});